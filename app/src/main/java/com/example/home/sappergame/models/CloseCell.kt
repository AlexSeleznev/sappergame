package com.example.home.sappergame.models

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TableRow
import com.example.home.sappergame.R


class CloseCell(override val x:Int, override val y:Int) : Cell {

    override var isOpen: Boolean = false

    override var isMine: Boolean = false

    override var neighborhoodBomb: Int =0


    override fun drawCell(context: Context): View {

        val b =  Button(context).apply {
            text = "<>"
            background = context.resources.getDrawable(R.drawable.close_button_selector)
        }

        b.layoutParams = TableRow.LayoutParams(100, 100)
        return b
    }


}