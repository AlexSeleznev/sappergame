package com.example.home.sappergame.data.managers

import com.example.home.sappergame.data.storage.GameStat
import com.example.home.sappergame.data.storage.GameStatDto
import io.realm.Realm
import io.realm.RealmResults

class RealmManager private constructor(){

    companion object {
        val INSTANCE = RealmManager()
    }

    fun saveStatToRealm(dto : GameStatDto){
        val realm = Realm.getDefaultInstance()
        val statRealm = GameStat(dto.gameTime, dto.gameColumns, dto.gameRows, dto.gameMinesCount, dto.isWon)
        realm.executeTransaction { realm -> realm.insertOrUpdate(statRealm) }
        realm.close()
    }

    fun getStatFromRealm():RealmResults<GameStat>{
        val realm = Realm.getDefaultInstance()
        return realm.where(GameStat::class.java).findAll()
    }

}