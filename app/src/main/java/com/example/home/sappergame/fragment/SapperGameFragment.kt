package com.example.home.sappergame.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import com.example.home.sappergame.R
import com.example.home.sappergame.SapperGameController
import com.example.home.sappergame.activities.RootActivity
import com.example.home.sappergame.models.Cell
import kotlinx.android.synthetic.main.fragment_game.*

class SapperGameFragment : Fragment(){
    lateinit var game : SapperGameController
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_game, container, false)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as RootActivity).showGameToolbar()
        startGame()
        }

     fun startGame(){
        game = SapperGameController().also {
            it.initView(this)
            it.startGame()
        }
    }

    fun showGameField(field: Array<Array<Cell>>) {
        game_table_layout.removeAllViews()
        for(column in field){
            val row = TableRow(activity).apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)
            }
            for(rows in column){
                val b = rows.drawCell(activity!!)
                b.setOnClickListener { game.openCellByPosition(rows.x, rows.y) }
                b.setOnLongClickListener { game.setFlag(rows) }
                row.addView(b)
            }
            game_table_layout.addView(row)
        }
    }

    override fun onPause() {
        super.onPause()
        game.stopTimer()
    }

    fun setSecondText(toString: String) {
        activity?.runOnUiThread({
            timer_tv.text = String.format("%02d:%02d", toString.toInt()/60, toString.toInt()%60)
        })

    }
}