package com.example.home.sappergame.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.ActionBar
import com.example.home.sappergame.R
import com.example.home.sappergame.fragment.MainScreenFragment
import com.example.home.sappergame.fragment.SapperGameFragment
import kotlinx.android.synthetic.main.activity_root.*
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.util.TypedValue
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import com.example.home.sappergame.fragment.SettingsFragment


class RootActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var filedRows=9
    var fieldColumns = 9
    var minesCount = fieldColumns*filedRows/4
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment : Fragment? = null
        when(item.itemId){
            R.id.settings_drawer->fragment = SettingsFragment()
            R.id.exit_drawer-> finish()
            R.id.statistic_drawer->{  }
        }
        supportFragmentManager.beginTransaction().replace(R.id.main_container, fragment?:MainScreenFragment()).addToBackStack(null).commit()

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private lateinit var actionBar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        supportFragmentManager.beginTransaction().add(R.id.main_container, MainScreenFragment()).commit()
        initToolbar()
        initNavigationDrawer()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        actionBar = supportActionBar!!
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
    }

    fun showGameToolbar(){
        if (supportActionBar != null) {
            supportActionBar!!.show()
        }
        var actionBarHeight = 0
        val tv = TypedValue()
        if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
        }
        val layoutParams = main_container.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.setMargins(0, actionBarHeight, 0, 0)
        main_container.layoutParams = layoutParams
    }

    fun hideToolBar(){
        actionBar.hide()
        val layoutParams = main_container.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.setMargins(0, 0, 0, 0)
        main_container.layoutParams = layoutParams
        main_container.fitsSystemWindows = true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home->{  }
            R.id.new_game->{ (supportFragmentManager.findFragmentByTag("game")as SapperGameFragment).startGame() }
            else-> return super.onOptionsItemSelected(item)
        }
        return false
    }

    private fun initNavigationDrawer() {
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.drawerlayoup_open_title, R.string.drawerlayout_close_title)
        drawer_layout.setDrawerListener(toggle)
        toggle.syncState()
        navigation_view.setNavigationItemSelectedListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.game_menu, menu)
        return true
    }

}
