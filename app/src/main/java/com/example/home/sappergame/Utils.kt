package com.example.home.sappergame


class Utils {
    companion object {
        fun calculateMines(rows: Int, columns: Int, mines: Int): Boolean = (rows*columns/3)<=mines
    }}