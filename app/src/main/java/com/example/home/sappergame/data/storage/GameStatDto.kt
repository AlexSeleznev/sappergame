package com.example.home.sappergame.data.storage

import java.util.*


data class GameStatDto(val gameTime : Date,
                  val gameColumns : Int,
                  val gameRows : Int,
                  val gameMinesCount : Int,
                  val isWon : Boolean)