package com.example.home.sappergame

import android.widget.Toast
import com.example.home.sappergame.activities.RootActivity
import com.example.home.sappergame.data.managers.RealmManager
import com.example.home.sappergame.data.storage.GameStatDto
import com.example.home.sappergame.fragment.SapperGameFragment
import com.example.home.sappergame.models.*
import java.util.*

class SapperGameController {
    private lateinit var field: Array<Array<Cell>>
    lateinit var view: SapperGameFragment
    private lateinit var activity: RootActivity
    private val realmManager = RealmManager.INSTANCE
    private var countMines = 0
    private val random = Random()
    private var openCellCount = 0
    private var bangMine = false
    private var youWon = false
    private var timer: Timer
    var seconds = 0

    init {
        timer = Timer()
    }

    fun initView(view: SapperGameFragment) {
        this.view = view
    }

    fun startGame() {
        activity = view.activity as RootActivity
        field = Array(activity.filedRows, init = { column -> Array<Cell>(activity.fieldColumns, init = { row -> CloseCell(column, row) }) })

        while (countMines < activity.minesCount) {
            var x: Int
            var y: Int
            do {
                x = random.nextInt(activity.filedRows - 1)
                y = random.nextInt(activity.fieldColumns - 1)
            } while (field[x][y].isMine)
            field[x][y].isMine = true
            countMines++
        }

        for (x in 0 until activity.filedRows) {
            for (y in 0 until activity.fieldColumns) {
                if (!field[x][y].isMine) {
                    var count = 0
                    for (dx in -1..1) {
                        for (dy in -1..1) {
                            var nx = x + dx
                            var ny = y + dy
                            if (nx < 0 || ny < 0 || nx > activity.filedRows - 1 || ny > activity.fieldColumns - 1) {
                                nx = x
                                ny = y
                            }
                            count += if (field[nx][ny].isMine) 1 else 0
                        }
                    }
                    field[x][y].neighborhoodBomb = count
                }
            }
        }

        if (timer != null) {
            timer.cancel()
            timer.purge()
        }
        seconds = 0
        timer = Timer()
        view.showGameField(field)
        timer.schedule(MyTimerTask(), 0, 1000)
    }

    private fun openCell(rows: Cell) {
        val x = rows.x
        val y = rows.y

        if (field[x][y].isMine && field[x][y] is CloseCell) {
            field[x][y] = BombCell(x, y)
            bangMine = true
        }

        if (field[x][y] is FlagCell) field[x][y] = CloseCell(x, y).apply {
            isMine = field[x][y].isMine
            neighborhoodBomb = field[x][y].neighborhoodBomb
        }
        else if (field[x][y] is CloseCell) {
            val bombs = (field[x][y] as CloseCell).neighborhoodBomb
            val openCell = OpenCell(x, y).apply { neighborhoodBomb = bombs }
            field[x][y] = openCell
        }

        if (!field[x][y].isMine) openCellCount++

        if (field[x][y].isMine && field[x][y] is BombCell) {
            Toast.makeText(view.activity, view.getString(R.string.lose_title), Toast.LENGTH_LONG).show()
            realmManager.saveStatToRealm(GameStatDto(Date(), activity.fieldColumns, activity.filedRows, activity.minesCount, false))
            timer.cancel()
        }
        if (openCellCount == (activity.filedRows*activity.fieldColumns) - activity.minesCount) {
            Toast.makeText(view.activity, view.getString(R.string.win_title), Toast.LENGTH_LONG).show()
            youWon = true
        }
        view.showGameField(field)

    }

    fun openCellByPosition(cellX: Int, cellY: Int) {
        val x = cellX
        val y = cellY
        if (!bangMine && !youWon) {
            if (x < 0 || x > activity.filedRows - 1 || y < 0 || y > activity.fieldColumns - 1) return
            if (field[x][y] is OpenCell) return
            if (field[x][y] is FlagCell) {
                openCell(field[x][y])
                return
            }
            openCell(field[x][y])
            if (field[x][y].neighborhoodBomb > 0 || field[x][y].isMine) return
            for (dx in -1..1) {
                for (dy in -1..1) {
                    openCellByPosition(x + dx, y + dy)
                }
            }
        }
    }

    fun setFlag(rows: Cell): Boolean {
        return if (!bangMine && !youWon) {
            if (field[rows.x][rows.y] is CloseCell) {
                field[rows.x][rows.y] = FlagCell(rows.x, rows.y).apply {
                    isMine = field[rows.x][rows.y].isMine
                    neighborhoodBomb = field[rows.x][rows.y].neighborhoodBomb
                }
            }
            view.showGameField(field)
            true
        } else false

    }

    inner class MyTimerTask : TimerTask() {

        override fun run() {
            seconds++
            view.setSecondText(seconds.toString())

        }

    }

    fun stopTimer() {
        timer.cancel()
    }

}