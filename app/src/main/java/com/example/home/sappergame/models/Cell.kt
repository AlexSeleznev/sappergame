package com.example.home.sappergame.models

import android.content.Context
import android.view.View


interface Cell {
    val x:Int
    val y:Int
    var isMine:Boolean
    var isOpen: Boolean
    var neighborhoodBomb :Int

    fun drawCell(context:Context): View
}