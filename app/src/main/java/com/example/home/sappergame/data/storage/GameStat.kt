package com.example.home.sappergame.data.storage

import io.realm.RealmObject
import java.util.*

open class GameStat() : RealmObject() {

    var gameTime = Date()
    var gameColumns = 0
    var gameRows = 0
    var gameMinesCount = 0
    var isWon : Boolean = false

    constructor(gameTime : Date,
                gameColumns : Int,
                gameRows : Int,
                gameMinesCount : Int,
                isWon : Boolean) : this(){
        this.gameTime = gameTime
        this.gameColumns = gameColumns
        this.gameRows = gameRows
        this.gameMinesCount = gameMinesCount
        this.isWon = isWon
    }
}