package com.example.home.sappergame.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.home.sappergame.R
import com.example.home.sappergame.Utils
import com.example.home.sappergame.activities.RootActivity
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        save_button.setOnClickListener{
            val rows = weigth_et.text.toString().toInt()
            val columns = height_et.text.toString().toInt()
            val mines =  mines_count_et.text.toString().toInt()
            if(columns>9 ||columns<2 ||rows >9 ||rows<2){
                Toast.makeText(activity, getString(R.string.error_field_size_title), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            (activity as RootActivity).apply {

                if(!Utils.calculateMines(rows, columns, mines)){
                    filedRows = rows
                    fieldColumns = columns
                    minesCount = mines
                    supportFragmentManager.popBackStack()
                }
                else Toast.makeText(this, getString(R.string.error_mine_count_hint), Toast.LENGTH_LONG).show()
                }
            }
        }
    }