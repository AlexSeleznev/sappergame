package com.example.home.sappergame.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.home.sappergame.R
import com.example.home.sappergame.activities.RootActivity
import kotlinx.android.synthetic.main.fragment_main_screen.*


class MainScreenFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_main_screen, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as RootActivity).hideToolBar()
        start_button.setOnClickListener { activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.main_container, SapperGameFragment(), "game")?.addToBackStack(null)?.commit() }
        settings_button.setOnClickListener { activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.main_container, SettingsFragment(), "settings")?.addToBackStack(null)?.commit() }
        exit_button.setOnClickListener { activity?.finish() }
    }

}